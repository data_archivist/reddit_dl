#!/usr/bin/python

from archive_reddit import archive_comments, archive_sub, init_archiver
import argparse

parser = argparse.ArgumentParser()


parser.add_argument("--comments", "-c", default=False, action="store_true",
                    help="Archive comments")
parser.add_argument("--posts", "-p", default=False, action="store_true",
                    help="Archive posts (submissions)")
parser.add_argument("sub",
                    help="subreddit name")
parser.add_argument("--threads", "-t", default=10, type=int,
                    help="Number of concurrent downloads")
parser.add_argument("--no_media", default=False, action="store_true",
                    help="Skip downloading external links")
parser.add_argument("--exclude_pattern", "-e", default="", type=str,
                    help="Regex pattern of urls to ingnore")


if __name__ == "__main__":

    args = parser.parse_args()

    if not args.comments and not args.posts:
        print("Nothing to do")

    init_archiver(args.threads, args.no_media, args.exclude_pattern)

    if args.posts:
        print("Archiving posts for " + args.sub + " with " + str(args.threads) + " threads")
        archive_sub(args.sub)

    if args.comments:
        print("Archiving comments for " + args.sub + " with " + str(args.threads) + " threads")
        archive_comments(args.sub)
