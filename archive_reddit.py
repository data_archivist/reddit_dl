#!/usr/bin/python

from psaw import PushshiftAPI
import os
import json
import re
from youtube_dl import YoutubeDL
from subprocess import run, DEVNULL
from queue import Queue, Empty
from threading import Thread
import logging

log = logging.Logger("yt_dl")
log.addHandler(logging.NullHandler())

api = PushshiftAPI()
q = Queue()
workers = list()
processed = set()

skipMedia = False
excludePattern = re.compile(r"(reddit.com/r)")

LINK_PATTERN = re.compile(r'\[.*\]\(([^)]+)\)')
LINK_PATTERN2 = re.compile(r'(https?://[^\s)\],*\\]+)')


def load_processed():

    if os.path.exists("rdl_archive.txt"):
        with open("rdl_archive.txt") as f:
            processed.update([x.strip() for x in f.readlines()])


def mark_processed(item):
    with open("rdl_archive.txt", "a") as f:
        f.write(item.id + "\n")
    processed.add(str(item.id))


def download_media(url, dirname):
    if url.startswith("/"):
        return
    try:
        yt_dl = YoutubeDL(params={
            "format": "bestvideo+bestaudio/best",
            "outtmpl": dirname + "/%(extractor)s/%(title)s.%(ext)s",
            "quiet": True,
            "logger": log,
            "noplaylist": True,
            "download_archive": "ytdl_archive.txt",
            "writeinfojson": True,
        })
        yt_dl.download([url, ])
    except Exception:
        run(["gallery-dl", url, "-o", "base-directory=" + dirname, "-o", "archive=gdl_archive.sqlite"],
            stderr=DEVNULL, stdout=DEVNULL)
    print("Media: " + url)


def extract_links(body):
    links = set()

    body = body.replace("\\)", "&#x28;")
    for match in LINK_PATTERN.finditer(body):
        links.add(match.group(1))

    for match in LINK_PATTERN2.finditer(body):
        links.add(match.group(1))

    return links


def archive_item(item):

    if str(item.id) in processed:
        return

    dirname = str(item.subreddit)

    meta_file = dirname + os.sep + type(item).__name__ + "_meta.mdjson"
    meta = json.dumps(item.d_)

    if not skipMedia:
        media_files = list()

        if type(item).__name__ == "submission":
            if item.is_self and hasattr(item, "selftext"):
                media_files.extend(extract_links(item.selftext))
            else:
                media_files.extend(item.url.split())
        elif type(item).__name__ == "comment":
            media_files.extend(extract_links(item.body))

        for url in media_files:
            if not excludePattern.match(url):
                download_media(url, item.subreddit)
            else:
                print("Skipped" + url)

    with open(meta_file, "a") as out:
        out.write(meta + "\n")

    mark_processed(item)


def worker():
    while True:
        try:
            item = q.get(timeout=10)
            archive_item(item)
            q.task_done()
        except Empty:
            break


def setup_threads(threads):
    for _ in range(threads):
        t = Thread(target=worker)
        t.start()


def init_archiver(threads, skip_media, exclude_pattern):

    global skipMedia
    global excludePattern

    setup_threads(threads)
    load_processed()

    skipMedia = skip_media
    if exclude_pattern:
        excludePattern = re.compile(exclude_pattern)


def archive_sub(sub):
    if not os.path.exists(sub):
        os.mkdir(sub)
    gen = api.search_submissions(subreddit=sub, sort="asc")
    for sub in gen:
        q.put(sub)
    q.join()


def archive_comments(sub):
    if not os.path.exists(sub):
        os.mkdir(sub)
    gen = api.search_comments(subreddit=sub, sort="asc")
    for comment in gen:
        q.put(comment)
    q.join()
