Multi-threaded Python command line tool for archiving Reddit posts and comments metadata and media. Uses youtube-dl and gallery-dl for external media.
Reddit data is pulled from pushshift.io API using the psaw wrapper.

## Installation
```bash
sudo pip install -r requirements.txt
python cmdline.py {options...}

```

## Usage
```
usage: cmd_line.py [-h] [--comments] [--posts] [--threads THREADS]
                   [--no_media] [--exclude_pattern EXCLUDE_PATTERN]
                   sub

positional arguments:
  sub                   subreddit name

optional arguments:
  -h, --help            show this help message and exit
  --comments, -c        Archive comments
  --posts, -p           Archive posts (submissions)
  --threads THREADS, -t THREADS
                        Number of concurrent downloads
  --no_media            Skip downloading external links
  --exclude_pattern EXCLUDE_PATTERN, -e EXCLUDE_PATTERN
                        Regex pattern of urls to ingnore

```